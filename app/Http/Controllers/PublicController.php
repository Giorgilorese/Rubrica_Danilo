<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller

{
    
	public function index(Request $req)

	{

		$rubrica = $req->session()->get('rubrica'); 

        if (is_array($rubrica)) {
            
            $count = count($rubrica);

        }
        else{

            $rubrica = [];
            $count = 0;

        }

		return view('welcome', ['contacts'=>$rubrica, 'count'=>$count]);

	}
    
    public function contactNew(Request $req)

    {

    	$name = $req->input('name');
    	$surname = $req->input('surname');
    	$mobile = $req->input('mobile');
        $email = $req->input('email');


    	$contatto = ['name'=>$name, 'surname'=>$surname, 'mobile'=>$mobile, 'email' =>$email];

    	$req->session()->push('rubrica', $contatto);

    	return redirect('/');

    }

    public function contactDetail(Request $req, $id)
    {
        
        $rubrica = $req->session()->get('rubrica');
        $contact = $rubrica[$id] ?? null;
        
        if(empty($contact))
            abort(404);
        else
            return view('detail', ['contact'=>$contact, 'id'=>$id]);

    } 


    public function contactDelete(Request $req, $id)
    {

        $rubrica = $req->session()->get('rubrica'); 

        array_splice($rubrica, $id, 1);

        $req->session()->put('rubrica', $rubrica);

        return redirect('/');

    }

    public function contactEdit(Request $req, $id)
   {

        $rubrica = $req->session()->get('rubrica');
        $contact = $rubrica[$id];

        return view('edit', ['contact'=>$contact, 'id'=>$id]);

   } 

   public function contactUpdate(Request $req, $id)

   {

        $name = $req->input('name');
        $surname = $req->input('surname');
        $mobile = $req->input('mobile');
        $email = $req->input('email');


        $contatto = ['name'=>$name, 'surname'=>$surname, 'mobile'=>$mobile, 'email' =>$email];
        $rubrica = $req->session()->get('rubrica');
        $rubrica[$id] = $contatto;

        $req->session()->put('rubrica', $rubrica);

        return redirect('/');

   }

}
