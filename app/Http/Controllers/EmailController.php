<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\make_mail;


class EmailController extends Controller
{

	public function mailview(){

		return view('mail');
	}

	public function send(Request $request){
		
		$data=$request->except('_token');
		//dd($contact);
		Mail::to($data["email"])->send(new make_mail($data));
		return redirect("/");
	}


   /* public function send(Request $request){
    
    	 public function send(Request $request)
	    {
	        $title = $request->input('title');
	        $content = $request->input('content');

	        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
	        {

	            $message->from('me@gmail.com', 'Christian Nwamba');

	            $message->to('chrisn@scotch.io');
xattr_get(filename, name)
	        });

	        return response()->json(['message' => 'Request completed']);
	    }



}*/

}
