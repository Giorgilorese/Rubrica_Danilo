<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');

Route::post('/contacts/new', 'PublicController@contactNew')->name('contacts.new');

Route::get('/contacts/{id}', 'PublicController@contact1Detail')
	->name('detail')
	->where('id', '[0-9]+');


Route::delete('/contacts/delete/{id}', 'PublicController@contactDelete')
	->name('contacts.delete')
	->where('id', '[0-9]+');

Route::get('/contacts/edit/{id}', 'PublicController@contactEdit')
	->name('edit')
	->where('id', '[0-9]+');

Route::put('/contacts/edit/{id}', 'PublicController@contactUpdate')
	->name('update')
	->where('id', '[0-9]+');

Route::get('/mail', 'EmailController@mailview');
Route::post('/send', 'EmailController@send')->name('sendmail');


Route::get('/home', 'HomeController@index')->name('home');



Auth::routes();


