@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modifica il contatto in rubrica</div>

                <div class="card-body">

                   @include('_edit-form')
                
                </div>
            </div>
        </div>
    </div>

    <br><br>

</div>
@endsection


